const {loadPersons} = require('./person-fixtures');
const Person = require('../src/entity/person');

loadPersons().then(() => {
    console.log('persons loaded');
    Person.sequelize.close();
});