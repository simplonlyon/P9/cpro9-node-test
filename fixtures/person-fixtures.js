const Person = require('../src/entity/person');

async function loadPersons() {
    await Person.drop();
    await Person.sync();
    let persons = []
    for(let x = 1; x <= 5; x++) {
        persons.push({
            firstName: 'FirstName'+x,
            lastName: 'LastName'+x,
            age: x*10
        });
    }
    await Person.bulkCreate(persons);
    
}

module.exports = {
    loadPersons
};