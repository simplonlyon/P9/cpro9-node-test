const {Router} = require('express');
const Person = require('../entity/person');

const personRouter = Router();

personRouter.get('/', async (req, res) => {
    try {
        const persons = await Person.findAll();
        res.json(persons);
    }catch {
        res.status(500).end();
    }
});

personRouter.get('/:id', async (req, res) => {
    try {
        const person = await Person.findByPk(req.params.id);
        if(!person) {
            return res.status(404).end();
        }
        res.json(person);
    }catch {
        res.status(500).end();
    }
});

personRouter.post('/', async (req, res) => {
    try {
        const person = await Person.create(req.body);
        res.status(201).json(person);
    }catch {
        res.status(500).end();
    }
});

personRouter.patch('/:id', async (req, res) => {
    try {
        const person = await Person.findByPk(req.params.id);
        if(!person) {
            return res.status(404).end();
        }
        await person.update(req.body);
        res.json(person);
    }catch {
        res.status(500).end();
    }
});

personRouter.delete('/:id', async (req, res) => {
    try {
        const person = await Person.findByPk(req.params.id);
        if(!person) {
            return res.status(404).end();
        }
        await person.destroy();
        res.status(204).end();
    }catch {
        res.status(500).end();
    }
});

module.exports = personRouter;