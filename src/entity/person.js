const {DATABASE_URL} = require('../../env');

const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize(DATABASE_URL, {
  logging: process.env.NODE_ENV !== 'test'
});


class Person extends Model {}

Person.init({
  // Model attributes are defined here
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  age: {
      type: DataTypes.INTEGER
  }
}, {
  
  sequelize, // We need to pass the connection instance
  modelName: 'Person' // We need to choose the model name
});

Person.sync();

module.exports = Person;