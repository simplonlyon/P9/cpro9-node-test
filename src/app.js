const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const personRouter = require('./router/person-router');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());

app.use('/api/persons', personRouter);





const server = app.listen(port, () => {
    console.log('Server listening on http://localhost:'+port);
});

module.exports = {
    app,
    server
}