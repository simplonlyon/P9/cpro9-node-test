let DATABASE_URL = 'mysql://root:1234@localhost:3306/cpro9_node_test';

if(process.env.NODE_ENV === 'test') {
    DATABASE_URL = 'sqlite::memory';
}

module.exports = {
    DATABASE_URL
};