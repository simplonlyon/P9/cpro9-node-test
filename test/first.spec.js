const {loadPersons} = require('../fixtures/person-fixtures');
const Person = require('../src/entity/person');

describe('Random test', () => {
    
    beforeEach(async () => {
        await loadPersons()
    })

    it('should pass', () => {
        expect(true).toBeTruthy();
    });

    it('should have 5 persons', async () => {
        const persons = await Person.findAll();
        expect(persons.length).toEqual(5);
    });

});
