const { loadPersons } = require('../fixtures/person-fixtures');
const Person = require('../src/entity/person');
const request = require('supertest');
const { app, server } = require('../src/app');

describe('Random test', () => {
    beforeEach(async () => {
        
        await loadPersons()
    });

    it('should fetch 5 persons', async () => {
        return request(app).get('/api/persons')
            .expect(200)
            .then(response => {
                const persons = response.body;
                expect(persons.length).toEqual(5);
                expect(persons[0].firstName).toEqual('FirstName1');
            });
    });

    it('should fetch 1 person by id', async () => {
        return request(app).get('/api/persons/1')
            .expect(200)
            .then(response => {
                const person = response.body;
                expect(person).toBeTruthy();
                expect(person.firstName).toEqual('FirstName1');
            });
    });
    
    it('should return 404 if no user', async () => {
        return request(app).get('/api/persons/10')
            .expect(404);
            
    });


    it('should add a new person', async () => {
        const response = await request(app).post('/api/persons')
            .send({
                firstName: 'Test',
                lastName: 'TestLast',
                age: 65
            })
            .expect(201)

        const person = response.body;
        expect(person.id).toEqual(6);
        const count = await Person.count();
        expect(count).toEqual(6);

        return;
    });
    
    it('should delete person with id 1', async () => {
        await request(app).delete('/api/persons/1')
            .expect(204)

        const count = await Person.count();
        expect(count).toEqual(4);

        return;
    });

    
    it('should update person with id 1', async () => {
        const response = await request(app).patch('/api/persons/1')
            .send({
                firstName: 'Test'
            })
            .expect(200)

        const person = response.body;
        expect(person.firstName).toEqual('Test');
        const fromDb = await Person.findByPk(1);
        expect(fromDb.firstName).toEqual('Test');
        expect(fromDb.lastName).toEqual('LastName1');
        expect(fromDb.age).toEqual(10);


        return;
    });


    afterAll(() => {
        server.close();
    });

});

