# Projet Node Pour Test

Un petit projet node.js / express / sequelize pour faire des test avec le framework Jest


## How To Use
1. Créer une base de donnée sql et modifier les informations de connexion dans env.js
2. `npm run dev` pour lancer le serveur de développement
3. `npm test` pour lancer les tests du projet
4. `npm run load:fixtures` pour lancer les fixtures